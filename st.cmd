require essioc
require adsis8300bpm
epicsEnvSet("PVXS_QSRV_ENABLE", "NO")
require ndpluginsds
epicsEnvSet("USE_SDS", "")

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM03")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-140")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-3")
epicsEnvSet("EVR_NAME",       "PBI-BPM03:Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "SPK-120LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "SPK 120 LWU BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "SPK-130LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "SPK 130 LWU BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

